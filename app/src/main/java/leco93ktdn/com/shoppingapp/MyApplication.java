package leco93ktdn.com.shoppingapp;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import leco93ktdn.com.shoppingapp.data.model.ProductModel;

/**
 * Created by Gcb Le Co.
 */
public class MyApplication extends Application {
    public static List<ProductModel> shoppingCart = new ArrayList<>();
}
