package leco93ktdn.com.shoppingapp.data.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Gcb Le Co.
 */
public class ResponseModel {

    @SerializedName("data")
    @Expose
    private JsonElement data = null;

    @SerializedName("count")
    @Expose
    private Integer count;

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonElement data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}