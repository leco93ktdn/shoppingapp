package leco93ktdn.com.shoppingapp.data.remote;

import leco93ktdn.com.shoppingapp.data.model.ResponseModel;
import retrofit2.Response;

/**
 * Created by Gcb Le Co.
 */
public interface IHttpResponse {

    void onHttpComplete(Response<ResponseModel> response, int idRequest);

    void onHttpError(String response, int idRequest, int errorCode);

}
