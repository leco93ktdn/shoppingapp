package leco93ktdn.com.shoppingapp.data.remote;

import com.google.gson.JsonObject;

import androidx.annotation.NonNull;
import leco93ktdn.com.shoppingapp.data.model.ResponseModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gcb Le Co.
 */
public class NetworkManager {

    private IHttpResponse iHttpResponse;

    public NetworkManager(IHttpResponse iHttpResponse) {
        this.iHttpResponse = iHttpResponse;
    }

    //=====================================================================================
    public void requestApi(final Call<ResponseModel> call, final int idRequest) {
        if (call == null) {
            return;
        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<ResponseModel> call, @NonNull Response<ResponseModel> response) {
                int status = response.raw().code();
                if (status == 200 || status == 201) {//200 -> get, 201 -> post
                    iHttpResponse.onHttpComplete(response, idRequest);
                } else {
                    iHttpResponse.onHttpError(response.message(), idRequest, status);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseModel> call, @NonNull Throwable t) {
                iHttpResponse.onHttpError(t.getMessage(), idRequest, 0);
            }
        });
    }

    public Call<ResponseModel> login(String email, String password) {
        MyEndPoint apiMainService = ServiceGenerator.createServiceMain(MyEndPoint.class);
        JsonObject data = new JsonObject();
        data.addProperty("email", email);
        data.addProperty("password", password);
        return apiMainService.postLogin(data);
    }

    public Call<ResponseModel> products() {
        MyEndPoint apiMainService = ServiceGenerator.createServiceMain(MyEndPoint.class);
        return apiMainService.getProducts();
    }
}
