package leco93ktdn.com.shoppingapp.data.remote;

import com.google.gson.JsonObject;

import leco93ktdn.com.shoppingapp.data.model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Gcb Le Co.
 */
public interface MyEndPoint {

    @POST("api/login")
    Call<ResponseModel> postLogin(@Body JsonObject data);

    @GET("api/products")
    Call<ResponseModel> getProducts();
}
