package leco93ktdn.com.shoppingapp.data.local;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Gcb Le Co.
 */
public class PreferencesHelper {
    private static final String APP_PREFS = "pref_shopping_app";

    //------------------------------------------
    public static String getString(Context mContext, String key) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        return mSharedPreferences.getString(key, null);
    }

    public static String getString(Context mContext, String key, String defaultValue) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        return mSharedPreferences.getString(key, defaultValue);
    }

    public static void saveString(Context mContext, String key, String value) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putString(key, value);
        mEditor.apply();
    }

    //------------------------------------------
    public static int getInt(Context mContext, String key) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        return mSharedPreferences.getInt(key, 0);
    }

    public static int getInt(Context mContext, String key, int defaultValue) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        return mSharedPreferences.getInt(key, defaultValue);
    }

    public static void saveInt(Context mContext, String key, int value) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putInt(key, value);
        mEditor.apply();
    }

    //------------------------------------------
    public static boolean getBoolean(Context mContext, String key) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        return mSharedPreferences.getBoolean(key, false);
    }

    public static boolean getBoolean(Context mContext, String key, boolean defaultValue) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    public static void saveBoolean(Context mContext, String key, boolean value) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean(key, value);
        mEditor.apply();
    }

    //------------------------------------------
    public static void clear(Context mContext) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        mSharedPreferences.edit().clear().apply();
    }

    public static void remove(Context mContext, String key) {
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        mSharedPreferences.edit().remove(key).apply();
    }
}
