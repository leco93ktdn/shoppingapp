package leco93ktdn.com.shoppingapp.utils;

/**
 * Created by Gcb Le Co.
 */
public class AppConstants {

    //api
    public static final int ID_POST_LOGIN = 0;
    public static final int ID_POST_REGISTER = 1;
    public static final int ID_GET_PRODUCTS = 2;

    //save data
    public static final String PREF_REMEMBER = "PREF_REMEMBER";
    public static final String PREF_EMAIL = "PREF_EMAIL";
    public static final String PREF_PASSWORD = "PREF_PASSWORD";
    public static final String PREF_NAME = "PREF_NAME";
    public static final String PREF_AVATAR = "PREF_AVATAR";
}
