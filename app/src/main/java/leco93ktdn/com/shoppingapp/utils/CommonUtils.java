package leco93ktdn.com.shoppingapp.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import id.zelory.compressor.Compressor;
import leco93ktdn.com.shoppingapp.R;

/**
 * Created by Gcb Le Co.
 */
public class CommonUtils {

    public static String subPrice(int number) {
        return "¥ " + NumberFormat.getNumberInstance(Locale.getDefault()).format(number);
    }

    //==================================================================
    public static void ShowDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(title)
                .setTitle(message)//no network connection
                .setCancelable(false)
                .setNegativeButton(R.string.ok, (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    //==================================================================
    public static File compressFile(Context context, File actualImageFile) {
        try {
            return new Compressor(context).compressToFile(actualImageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //==================================================================
    public static void hideKeyboard(AppCompatActivity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            view.clearFocus();
        }
    }

    //==========================================================================
    public static void loadImageGlide(Context mContext, String uri, ProgressBar progressBar, ImageView imageView) {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
        RequestOptions myOptions = new RequestOptions()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder_square);

        Glide.with(mContext).load(uri).apply(myOptions).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
                return false;
            }
        }).into(imageView);
    }

    //==========================================================================
    public interface Callback<T> {
        void run(T obj);
    }
}
