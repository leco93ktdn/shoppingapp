package leco93ktdn.com.shoppingapp.ui.product.product_detail;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import at.blogc.android.views.ExpandableTextView;
import leco93ktdn.com.shoppingapp.MyApplication;
import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.data.model.ProductModel;
import leco93ktdn.com.shoppingapp.ui.base.BaseFragment;
import leco93ktdn.com.shoppingapp.ui.product.product_detail.show_cart.ShowCartFragment;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;

/**
 * Created by Gcb Le Co.
 */
public class ProductDetailFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = ProductDetailFragment.class.getSimpleName();

    private ImageButton btnBack;
    private TextView txtTitle;

    private ImageButton buttonToggle, btnIncrease, btnDecrease;
    private ExpandableTextView txtDescription;

    private TextView txtProductPrice;
    private ImageView imgProduct;
    private ProgressBar progressBar;
    private Button btnAddCart, btnBuyNow;
    private TextView txtQuantity;
    private ProductModel productModel;

    public static ProductDetailFragment newInstance(ProductModel model) {
        Bundle args = new Bundle();
        args.putParcelable(ProductDetailFragment.TAG, model);
        ProductDetailFragment fragment = new ProductDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productModel = getArguments().getParcelable(ProductDetailFragment.TAG);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBack = view.findViewById(R.id.btnBack);
        txtTitle = view.findViewById(R.id.txtTitle);

        txtProductPrice = view.findViewById(R.id.txtProductPrice);
        imgProduct = view.findViewById(R.id.imgProductDetail);
        progressBar = view.findViewById(R.id.progressBar);
        txtDescription = view.findViewById(R.id.txtDescription);
        buttonToggle = view.findViewById(R.id.button_toggle);

        btnIncrease = view.findViewById(R.id.btnIncrease);
        btnDecrease = view.findViewById(R.id.btnDecrease);
        txtQuantity = view.findViewById(R.id.txtQuantity);
        btnAddCart = view.findViewById(R.id.btnAddCart);
        btnBuyNow = view.findViewById(R.id.btnBuyNow);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupExpandableTextView();

        if (productModel != null) {
            // Set default value for quantity
            productModel.setQuantity(1);
            txtQuantity.setText(String.valueOf(1));

            txtTitle.setText(productModel.getName());
            txtProductPrice.setText(getString(R.string.product_price, productModel.getPrice()));
            txtDescription.setText(productModel.getDescription());

            //load image product
            CommonUtils.loadImageGlide(getContext(), productModel.getImage(), progressBar, imgProduct);

            btnBack.setOnClickListener(this);
            btnIncrease.setOnClickListener(this);
            btnDecrease.setOnClickListener(this);
            btnAddCart.setOnClickListener(this);
            btnBuyNow.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack: {
                onBackPressed();
                break;
            }
            case R.id.btnIncrease: {
                increaseQuantity();
                break;
            }
            case R.id.btnDecrease: {
                decreaseQuantity();
                break;
            }
            case R.id.btnAddCart: {
                setupAddCart();
                break;
            }
            case R.id.btnBuyNow: {
                setupAddCart();
                addFragment(ShowCartFragment.newInstance(), R.id.content, true, true, true);
                break;
            }
        }
    }

    private void setupExpandableTextView() {
        // set animation duration via code, but preferable in your layout files by using the animation_duration attribute
        txtDescription.setAnimationDuration(750L);

        // set interpolators for both expanding and collapsing animations
        txtDescription.setInterpolator(new OvershootInterpolator());

        // or set them separately
        txtDescription.setExpandInterpolator(new OvershootInterpolator());
        txtDescription.setCollapseInterpolator(new OvershootInterpolator());

        // but, you can also do the checks yourself
        buttonToggle.setOnClickListener(v -> {
            if (txtDescription.isExpanded()) {
                txtDescription.collapse();
                buttonToggle.setImageResource(R.drawable.ic_arrow_down);
            } else {
                txtDescription.expand();
                buttonToggle.setImageResource(R.drawable.ic_arrow_up);
            }
        });

        // listen for expand / collapse events
        txtDescription.addOnExpandListener(new ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(@NonNull final ExpandableTextView view) {
                Log.d("TAG", "ExpandableTextView expanded");
            }

            @Override
            public void onCollapse(@NonNull final ExpandableTextView view) {
                Log.d("TAG", "ExpandableTextView collapsed");
            }
        });
    }

    private void setupAddCart() {
        boolean hasInShoppingCart = false;
        for (ProductModel productTmp : MyApplication.shoppingCart) {
            if (productTmp.getId().equals(productModel.getId())) {
                productTmp.addQuantity(productModel.getQuantity());
                hasInShoppingCart = true;
                break;
            }
        }

        // Add new product in shopping cart
        if (!hasInShoppingCart) {
            MyApplication.shoppingCart.add(new ProductModel(productModel));
        }
    }

    private void decreaseQuantity() {
        int quantity = productModel.getQuantity();
        if (quantity > 1) {
            quantity = productModel.getQuantity() - 1;
            txtQuantity.setText(String.valueOf(quantity));
            productModel.setQuantity(quantity);
        }
    }

    private void increaseQuantity() {
        int quantity = productModel.getQuantity() + 1;
        txtQuantity.setText(String.valueOf(quantity));
        productModel.setQuantity(quantity);
    }
}