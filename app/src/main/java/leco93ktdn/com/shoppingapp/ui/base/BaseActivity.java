package leco93ktdn.com.shoppingapp.ui.base;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;

/**
 * Created by Gcb Le Co.
 */
@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    public void replaceFragment(Fragment fragment, int containViewId, boolean backstack, boolean hasAnimation, boolean isLeftToRight) {
        try {
            if (!isFinishing() && !isDestroyed()) {
                CommonUtils.hideKeyboard(this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                if (hasAnimation) {
                    if (!isLeftToRight)
                        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                    else
                        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                transaction.replace(containViewId, fragment);
                if (backstack)
                    transaction.addToBackStack(fragment.getTag());
                transaction.commitAllowingStateLoss();
            }
        } catch (Exception ignored) {
        }
    }

    //==================================================================
    public void addFragment(Fragment fragment, int containViewId, boolean backstack, boolean hasAnimation, boolean isLeftToRight) {
        try {
            if (!isFinishing() && !isDestroyed()) {
                CommonUtils.hideKeyboard(this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                if (hasAnimation) {
                    if (!isLeftToRight)
                        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                    else
                        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                transaction.add(containViewId, fragment);
                if (backstack)
                    transaction.addToBackStack(fragment.getTag());
                transaction.commitAllowingStateLoss();
            }
        } catch (Exception ignored) {

        }
    }

    //==================================================================
    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    //==================================================================
    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    //==================================================================
    @Override
    public void onBackPressed() {
        CommonUtils.hideKeyboard(this);
        super.onBackPressed();
    }

    //==================================================================
    public void startNewActivity(Class<?> cls, Bundle bundle) {
        CommonUtils.hideKeyboard(this);
        Intent i = new Intent(this, cls);
        if (bundle != null) {
            i.putExtras(bundle);
        }
        this.startActivity(i);
    }

//    //==================================================================
//    public void showSnackBar(String message) {
//        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
//        View sbView = snackbar.getView();
//        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(ContextCompat.getColor(this, android.R.color.white));
//        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        textView.setGravity(Gravity.CENTER_HORIZONTAL);
//        snackbar.show();
//    }

    //==================================================================
    public void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}
