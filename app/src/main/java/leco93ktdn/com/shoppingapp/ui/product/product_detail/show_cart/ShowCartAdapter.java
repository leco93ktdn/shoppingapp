package leco93ktdn.com.shoppingapp.ui.product.product_detail.show_cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.data.model.ProductModel;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;

/**
 * Created by Gcb Le Co.
 */
public class ShowCartAdapter extends ArrayAdapter<ProductModel> {

    private Context context;
    private int resource;
    private List<ProductModel> productModels;
    private CommonUtils.Callback callback;

    public ShowCartAdapter(Context context, int resource, @NonNull List<ProductModel> productModels, CommonUtils.Callback callback) {
        super(context, resource, productModels);
        this.context = context;
        this.resource = resource;
        this.productModels = productModels;
        this.callback = callback;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(resource, null);

        ImageView imgProductCart = row.findViewById(R.id.imgProductCart);
        TextView txtCartProductName = row.findViewById(R.id.txtCartProductName);
        TextView txtCartProductPrice = row.findViewById(R.id.txtCartProductPrice);
        TextView txtCartQuantity = row.findViewById(R.id.txtCartQuantity);
        ImageButton btnIncreaseCart = row.findViewById(R.id.btnIncrease);
        ImageButton btnDecreaseCart = row.findViewById(R.id.btnDecreaseCart);
        ImageButton btnDeleteCart = row.findViewById(R.id.btnDeleteCart);

        final ProductModel model = this.productModels.get(position);

        txtCartProductName.setText(model.getName());
//        DecimalFormat df = new DecimalFormat("#,####");
        txtCartProductPrice.setText(model.getPrice());
        txtCartQuantity.setText(model.getQuantity() + "");
//        imgProductCart.setImageResource(productModels.get(position).getImage());

//        row.setOnClickListener(v -> {
//            v.setTag(model);
//            callback.run(v.getTag());
//        });

        btnDecreaseCart.setOnClickListener(v -> {

        });

        return row;
    }
}
