package leco93ktdn.com.shoppingapp.ui.login;

import android.os.Bundle;

import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.ui.base.BaseActivity;

/**
 * Created by Gcb Le Co.
 */
public class SignUpActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }
}
