package leco93ktdn.com.shoppingapp.ui.product.product_detail.show_cart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import leco93ktdn.com.shoppingapp.MyApplication;
import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.data.model.ProductModel;
import leco93ktdn.com.shoppingapp.ui.base.BaseFragment;
import leco93ktdn.com.shoppingapp.ui.product.product_detail.ProductDetailFragment;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;

/**
 * Created by Gcb Le Co.
 */
public class ShowCartFragment extends BaseFragment {

    private ImageButton btnBack;
    private TextView txtTitle;
    private ListView lvCartProduct;
    private ShowCartAdapter showCartAdapter;
    private List<ProductModel> productModels;

    private TextView txtTotalValue;


    public static ShowCartFragment newInstance() {
        Bundle args = new Bundle();
        ShowCartFragment fragment = new ShowCartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        productModels = MyApplication.shoppingCart;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_cart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBack = view.findViewById(R.id.btnBack);
        txtTitle = view.findViewById(R.id.txtTitle);
        lvCartProduct = view.findViewById(R.id.lvCartProduct);
        txtTotalValue = view.findViewById(R.id.txtTotalValue);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupToolbar();
        setupAdapter();
    }


    private void setupAdapter() {
        CommonUtils.Callback callback = obj -> {
            ProductModel model = (ProductModel) obj;
            Fragment fragment = ProductDetailFragment.newInstance(model);
            addFragment(fragment, R.id.content, true, true, true);
        };

        showCartAdapter = new ShowCartAdapter(getContext(), R.layout.item_cart, productModels, callback);
        lvCartProduct.setAdapter(showCartAdapter);
    }

    private void setupToolbar() {
        btnBack.setOnClickListener(v -> onBackPressed());
        txtTitle.setText(getString(R.string.cart));
    }
}
