package leco93ktdn.com.shoppingapp.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import de.hdodenhof.circleimageview.CircleImageView;
import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.data.model.LoginModel;
import leco93ktdn.com.shoppingapp.ui.base.BaseActivity;
import leco93ktdn.com.shoppingapp.ui.login.LoginActivity;
import leco93ktdn.com.shoppingapp.ui.product.ProductFragment;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;

/**
 * Created by Gcb Le Co.
 */
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    //nav header main
    private CircleImageView imgAvatar;
    private ProgressBar progressBar;
    private Button btnLogin;
    private TextView txtName;
    private TextView txtEmail;

    private Fragment fragment = null;

    private LoginModel loginModel;

//    private String DATABASE_NAME = "dbProduct.db";
//    private static final String DB_PATH_SUFIX = "/databases/";
//    private SQLiteDatabase database = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //put bundle from LoginActivity
        loginModel = getIntent().getParcelableExtra(LoginActivity.TAG + "loginModel");

        addControls();
        addEvents();
    }

    private void addEvents() {
        //toolbar
        setSupportActionBar(toolbar);
        //disable title toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        //DrawerLayout
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        if (loginModel != null) {
            CommonUtils.loadImageGlide(this, loginModel.getAvatar(), progressBar, imgAvatar);
            txtName.setText(loginModel.getName());
            txtEmail.setText(loginModel.getEmail());
            btnLogin.setVisibility(View.GONE);
        }

        btnLogin.setOnClickListener(v -> {
            startNewActivity(LoginActivity.class, null);
        });

        initFragmentDefault();
    }

    private void initFragmentDefault() {
        fragment = ProductFragment.newInstance();
        replaceFragment(fragment, R.id.content, true, true, true);
    }

    private void addControls() {
        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        //nav header main
        View header = navigationView.getHeaderView(0);
        imgAvatar = header.findViewById(R.id.imgAvatar);
        progressBar = header.findViewById(R.id.progressBar);
        btnLogin = header.findViewById(R.id.btnLogin);
        txtName = header.findViewById(R.id.txtName);
        txtEmail = header.findViewById(R.id.txtEmail);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //navigation drawer
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.navProduct: {
                fragment = new ProductFragment();
                Toast.makeText(this, "Product", Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.navPoitManagement: {
                Toast.makeText(this, "PoitMannagement", Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.navHistory: {
                Toast.makeText(this, "History", Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.navPersonalDetails: {
                Toast.makeText(this, "PersonalDetails", Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.navNotification: {
                Toast.makeText(this, "Notification", Toast.LENGTH_SHORT).show();
                break;
            }
        }

        if (fragment != null) {
            replaceFragment(fragment, R.id.content, true, true, true);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    public List<ProductModel> showProductOnGridView() {
//        database = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
//        //Truy van bang Query
//        Cursor cursor = database.query("Product", null, null, null, null, null, null);
////        //Truy van bang RawQuery
////        Cursor cursor = database.rawQuery("Select * from Contact", null);
//        productModels.clear();
//        while (cursor.moveToNext()) {
//            int id = cursor.getInt(0);
//            String name = cursor.getString(1);
//            String price = cursor.getString(2);
////            int image = cursor.getInt(3);
//            productModels.add(new ProductModel(id, name, price, ""));
//        }
//        cursor.close();
//        return productModels;
//    }

//    private void copyDataFromAssetsToApp() {
//        File dbFile = getDatabasePath(DATABASE_NAME);
//        if (!dbFile.exists()) {
//            try {
//                CopyDataBaseFromAsset();
////                Toast.makeText(this, "Sao chep thanh cong", Toast.LENGTH_LONG).show();
//            } catch (Exception e) {
//                Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
//            }
//        }
//    }

//    private void CopyDataBaseFromAsset() {
//        try {
//            InputStream myInput = getAssets().open(DATABASE_NAME);
//            String outFileName = layDuongDanLuuTru();
//            File f = new File(getApplicationInfo().dataDir + DB_PATH_SUFIX);
//            if (!f.exists()) {
//                f.mkdir();
//            }
//            OutputStream myOutput = new FileOutputStream(outFileName);
//            byte[] buffer = new byte[1024];
//            int length;
//            while ((length = myInput.read(buffer)) > 0) {
//                myOutput.write(buffer, 0, length);
//            }
//            myOutput.flush();
//            myOutput.close();
//            myInput.close();
//        } catch (Exception ex) {
//            Log.e("Loi_Sao_Chep", ex.toString());
//        }
//    }

//    private String layDuongDanLuuTru() {
//        return getApplicationInfo().dataDir + DB_PATH_SUFIX + DATABASE_NAME;
//    }
}

