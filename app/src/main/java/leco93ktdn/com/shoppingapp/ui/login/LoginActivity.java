package leco93ktdn.com.shoppingapp.ui.login;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.data.local.PreferencesHelper;
import leco93ktdn.com.shoppingapp.data.model.LoginModel;
import leco93ktdn.com.shoppingapp.data.model.ResponseModel;
import leco93ktdn.com.shoppingapp.data.remote.IHttpResponse;
import leco93ktdn.com.shoppingapp.data.remote.NetworkManager;
import leco93ktdn.com.shoppingapp.ui.MainActivity;
import leco93ktdn.com.shoppingapp.ui.base.BaseActivity;
import leco93ktdn.com.shoppingapp.utils.AppConstants;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;
import retrofit2.Response;

/**
 * Created by Gcb Le Co.
 */
public class LoginActivity extends BaseActivity implements IHttpResponse {

    public static final String TAG = LoginActivity.class.getSimpleName();

    private EditText edtEmail, edtPassword;
    private Button btnLogin;
    private CheckBox cbxRemember;

    private NetworkManager networkManager;
    private Animation shakeAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //init
        networkManager = new NetworkManager(this);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        addControls();
        addEvents();
    }

    private void addControls() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        cbxRemember = findViewById(R.id.cbxRemember);
    }

    private void addEvents() {
        btnLogin.setOnClickListener(v -> {
            String email = edtEmail.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();
            CommonUtils.hideKeyboard(this);
            if (validate()) {
                networkManager.requestApi(networkManager.login(email, password), AppConstants.ID_POST_LOGIN);
            }
        });
    }


    private boolean validate() {
        boolean isValidate = true;
        if (TextUtils.isEmpty(edtEmail.getText())) {
            edtEmail.startAnimation(shakeAnimation);
            edtEmail.setError(getString(R.string.blank_field));
            isValidate = false;
        }
        if (TextUtils.isEmpty(edtPassword.getText())) {
            edtPassword.startAnimation(shakeAnimation);
            edtPassword.setError(getString(R.string.blank_field));
            isValidate = false;
        }
        return isValidate;
    }

    //api
    @Override
    public void onHttpComplete(Response<ResponseModel> response, int idRequest) {
        Gson gson = new Gson();
        switch (idRequest) {
            case AppConstants.ID_POST_LOGIN:
                if (response.body() != null) {
                    LoginModel model = gson.fromJson(response.body().getData(), LoginModel.class);
                    if (model != null) {
                        saveData(model);
                    }
                }
                break;

        }
    }

    //api
    @Override
    public void onHttpError(String response, int idRequest, int errorCode) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }

    private void saveData(LoginModel model) {
        if (cbxRemember.isChecked()) {
            PreferencesHelper.saveBoolean(this, AppConstants.PREF_REMEMBER, true);
        } else {
            PreferencesHelper.saveBoolean(this, AppConstants.PREF_REMEMBER, false);
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable(LoginActivity.TAG + "loginModel", model);
        startNewActivity(MainActivity.class, bundle);
        finish();
    }
}
