package leco93ktdn.com.shoppingapp.ui.personal_info;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import leco93ktdn.com.shoppingapp.R;

/**
 * Created by Gcb Le Co.
 */
public class MyAccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
    }
}
