package leco93ktdn.com.shoppingapp.ui.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import leco93ktdn.com.shoppingapp.MyApplication;
import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.data.model.ProductModel;
import leco93ktdn.com.shoppingapp.data.model.ResponseModel;
import leco93ktdn.com.shoppingapp.data.remote.IHttpResponse;
import leco93ktdn.com.shoppingapp.data.remote.NetworkManager;
import leco93ktdn.com.shoppingapp.ui.base.BaseFragment;
import leco93ktdn.com.shoppingapp.ui.product.product_detail.ProductDetailFragment;
import leco93ktdn.com.shoppingapp.ui.product.product_detail.show_cart.ShowCartFragment;
import leco93ktdn.com.shoppingapp.utils.AppConstants;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;
import retrofit2.Response;

/**
 * Created by Gcb Le Co.
 */
public class ProductFragment extends BaseFragment implements IHttpResponse {

    private GridView gvProduct;
    private List<ProductModel> productModels;
    private List<ProductModel> originalModels;
    private ProductAdapter productAdapter;

    //api
    private NetworkManager networkManager;

    public static ProductFragment newInstance() {
        Bundle args = new Bundle();
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //api
        networkManager = new NetworkManager(this);

        //init
        productModels = new ArrayList<>();
        originalModels = new ArrayList<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gvProduct = view.findViewById(R.id.gvProduct);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //set option menu
        setHasOptionsMenu(true);

        setupAdapter();

        //api
        networkManager.requestApi(networkManager.products(), AppConstants.ID_GET_PRODUCTS);
    }

    private void setupAdapter() {
        CommonUtils.Callback callback = obj -> {
            ProductModel model = (ProductModel) obj;
            Fragment fragment = ProductDetailFragment.newInstance(model);
            addFragment(fragment, R.id.content, true, true, true);
        };

        productAdapter = new ProductAdapter(getContext(), R.layout.item_product, productModels, callback);
        gvProduct.setAdapter(productAdapter);
    }

    //options menu
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.main, menu);

        MenuItem menuItem = menu.findItem(R.id.mnuSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                productAdapter.setFilter(filter(s));
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                productAdapter.setFilter(filter(s));
                return true;
            }
        });
    }

    //search
    private List<ProductModel> filter(String query) {
        final List<ProductModel> filterModels = new ArrayList<>();
        for (ProductModel model : originalModels) {
            final String name = model.getName().toLowerCase();
            final String price = model.getPrice().toLowerCase();

            if (name.contains(query.toLowerCase())) {
                filterModels.add(model);
            } else if (price.contains(query)) {
                filterModels.add(model);
            }
        }
        return filterModels;
    }

    //options menu
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.mnuCart) {
            if (MyApplication.shoppingCart.size() > 0) {
                addFragment(ShowCartFragment.newInstance(), R.id.content, true, true, true);
            } else {
                Toast.makeText(getContext(), getString(R.string.no_product_in_cart), Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.mnuHome) {
            replaceFragment(ProductFragment.newInstance(), R.id.content, false, true, true);

        } else if (id == R.id.mnuSearch) {
            productAdapter.setFilter(productModels);
        }

        return super.onOptionsItemSelected(item);
    }

    //api
    @Override
    public void onHttpComplete(Response<ResponseModel> response, int idRequest) {
        Gson gson = new Gson();
        switch (idRequest) {
            case AppConstants.ID_GET_PRODUCTS:
                Type type = new TypeToken<List<ProductModel>>() {
                }.getType();
                if (response.body() != null) {
                    List<ProductModel> models = gson.fromJson(response.body().getData(), type);
                    if (models != null) {
                        productAdapter.addAll(models);
                        originalModels.addAll(models);
                    }
                }
                break;
        }
    }

    //api
    @Override
    public void onHttpError(String response, int idRequest, int errorCode) {
        Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
    }
}
