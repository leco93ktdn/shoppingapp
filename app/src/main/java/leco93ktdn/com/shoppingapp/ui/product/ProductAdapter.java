package leco93ktdn.com.shoppingapp.ui.product;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import leco93ktdn.com.shoppingapp.R;
import leco93ktdn.com.shoppingapp.data.model.ProductModel;
import leco93ktdn.com.shoppingapp.utils.CommonUtils;

/**
 * Created by Gcb Le Co.
 */
public class ProductAdapter extends ArrayAdapter<ProductModel> {
    private Context context;
    private int resource;
    private List<ProductModel> productModels;

    private CommonUtils.Callback callback;

    ProductAdapter(Context context, int resource, List<ProductModel> productModels, CommonUtils.Callback callback) {
        super(context, resource, productModels);
        this.context = context;
        this.resource = resource;
        this.productModels = productModels;
        this.callback = callback;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(resource, null);

        ProgressBar progressBar = row.findViewById(R.id.progressBar);
        ImageView imgProduct = row.findViewById(R.id.imgProduct);
        TextView txtProductName = row.findViewById(R.id.txtProductName);
        TextView txtProductPrice = row.findViewById(R.id.txtProductPrice);

        ProductModel model = productModels.get(position);

        txtProductName.setText(model.getName());
        txtProductPrice.setText(context.getString(R.string.product_price, model.getPrice()));
        CommonUtils.loadImageGlide(context, model.getImage(), progressBar, imgProduct);
//        Glide.with(context).load(model.getImage()).into(imgProduct);

        row.setOnClickListener(v -> {
            v.setTag(model);
            callback.run(v.getTag());
        });
        return row;
    }

    void setFilter(List<ProductModel> models) {
        productModels.clear();
        productModels.addAll(models);
        notifyDataSetChanged();
    }
}



