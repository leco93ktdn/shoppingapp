package leco93ktdn.com.shoppingapp.ui.base;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

/**
 * Created by Gcb Le Co.
 */
public class BaseFragment extends Fragment {

    private BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    //=============================================================================================
    public void replaceFragment(Fragment fragment, int containViewId, boolean backstack, boolean hasAnimation, boolean isLeftToRight) {
        if (mActivity != null) {
            mActivity.replaceFragment(fragment, containViewId, backstack, hasAnimation, isLeftToRight);
        }
    }

    //=============================================================================================
    public void addFragment(Fragment fragment, int containViewId, boolean backstack, boolean hasAnimation, boolean isLeftToRight) {
        if (mActivity != null) {
            mActivity.addFragment(fragment, containViewId, backstack, hasAnimation, isLeftToRight);
        }
    }

    //=============================================================================================
    public void startNewActivity(Class<?> cls, Bundle bundle) {
        if (mActivity != null) {
            mActivity.startNewActivity(cls, bundle);
        }
    }

    //=============================================================================================
//    public void showSnackBar(String message) {
//        if (mActivity != null) {
//            mActivity.showSnackBar(message);
//        }
//    }

    //=============================================================================================
    public void onBackPressed() {
        if (mActivity != null) {
            mActivity.onBackPressed();
        }
    }

    //=============================================================================================
    public void clearBackStack() {
        if (mActivity != null) {
            mActivity.clearBackStack();
        }
    }
}
